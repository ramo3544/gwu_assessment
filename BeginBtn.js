import React, { Component } from 'react';

class BeginBtn extends Component {
    render() {
        return (
            <div>
                <ul className="nav justify-content-center">
                    <li className="nav-item">
                        <a href="GWUStudentinfo.html" className="btn btn-primary btn-lg active" role="button" aria-pressed="true">Begin</a>
                    </li>
                </ul>
            </div>
        );
    }
}
export default BeginBtn;