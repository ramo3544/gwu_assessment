import React, { Component } from 'react';

class Description extends Component {
    render() {
        return (
            <div className="row justify-content-md-center">
                 
                <div className="col-sm-8" object="margin-top:30px;">
                     <h4>Description of the evaluation</h4>
                    <p className="test-justify"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, 
                         adipisci earum ad laborum qui neque odio optio? 
                         Modi similique perspiciatis nulla tempore ipsum optio? Quas iusto quibusdam quo. Earum, vel?
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, sapiente. Libero accusamus nemo aspernatur eos ducimus quibusdam consequuntur tempore voluptas vero minima eaque ex veniam obcaecati debitis, corrupti delectus saepe!
                    </p>
                </div>

             </div>
        );
    }
}
export default Description;