import React, { Component } from 'react';

class GWULogo extends Component {
    render() {
        return (
            <div className="row justify-content-md-center">
                <div className="col-md-auto">
                        
                    <img src="GWUlogo.jpg" alt="GWU Logo"/>
                    <h1 className="text-center">GWU Course Evaluation</h1>

                </div>
            </div> 
        );
    }
}
export default GWULogo;