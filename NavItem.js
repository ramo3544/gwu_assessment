import React, { Component } from 'react';

class NavItem extends Component {
    render() {
        return (
            <div className="collapse navbar-collapse tb-20" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <a className="nav-link" href="GWUStudentinfo.html">Student Inforomation <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="GWUCourseEval.html">Course Evaluation</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="GWUInstructorEval.html">Instructor Evaluation</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="GWUOverallEval.html">Overall Evaluation</a>
                    </li>
                </ul>
                <form className="form-inline my-2 my-lg-0">
                    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                    <button className="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        );
    }
}
export default NavItem;