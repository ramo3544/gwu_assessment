import React from "react";
import axios from "axios";
import "./styles.css";

export default class Questions extends React.Component {
  constructor() {
    super();
    this.state = {
      sections: [],
      questions: [] //added this array for the answers to the questions. should rename it to responses
     };
  }
 handleQuestionChange(event){
   this.state.questions[event.target.name-1] = event.target.id; //equivalent to questions[name] = id
   //console.log(event.target); //uncomment this to see how I got name, and id attributes
   console.log(this.state.questions); //prints the array
 }
 
 createQuestion = (question, keyid) => {
  let rbs = [];
  let i = 0;
  let radioQuestions;
  let radioGroups;
  if (question.type === "radio") {

    radioGroups = question.values.map((val, index) => {
        let indexid = keyid + "_" + ++i;
        let p = {};
        p.type = "radio";
        p.name = question.questionId;
        p.id = indexid;
        p.autoComplete = "off";
        
        //added defaultValue attribute, onChange attribute
        let ib = <input defaultValue={this.state.questions} onChange={this.handleQuestionChange.bind(this)}htmlFor={indexid} key={indexid} {...p} />;
        return <div className="col" key={indexid + "_" + i}>
            {ib}  
            <label id={indexid} className="sr-only">
                {val}
            </label>
         </div> 
       });

    radioQuestions = <div className="row">
        <div className="col-md-8" key={keyid}>
            {question.question}
        </div>
        <div className="col-md-4 radio-group">
            <div className="row" style={{textAlign:'center'}}>
                {radioGroups}
            </div>
        </div>
    </div>

   return (
    <div className="col-md-12 question-row">
     {radioQuestions}
    </div>
   );
  } else if (question.type === "dropdown") {
   question.values.forEach((val, index) => {
    rbs.push(<option value={val}>{val}</option>);
   });
   let indexid = keyid + "_" + ++i;
   return (
    <div className="col-md-8">
     <div>
      <label htmlFor={indexid}>
       {question.question}
      </label>
     </div>
     <select id={indexid}>
      {rbs}
     </select>
    </div>
   );
  }
 };

 componentDidMount() {

    //josh make sure to change localhost to 8080
  axios.get(`http://localhost:3000/questions`).then(res => {
   const sections = res.data;
   this.setState({ sections });
  });
 }

 render() {
  return (
   <form style={{marginLeft:'5%',marginRight:'5%'}} key="surveyquestions">
    {this.state.sections.map((section, index) => {
     return (
        <table className="col-md-12 table-striped">
            <thead className="row" style={{paddingTop:'45px'}}>
                <tr style={{color:'darkblue', fontSize:'19px', paddingBottom:'20px'}} className="col-md-8" key={section.sectionid}>
                    <th>{section.title}</th>
                </tr>
                <tr className="col-md-4" style={{fontSize:'13px', textAlign:'center'}}>
                    <th className="row">
                        <div className="col">Strongly Disagree</div>
                        <div className="col">Disagree</div>
                        <div className="col">Neutral</div>
                        <div className="col">Agree</div>
                        <div className="col">Strongly Agree</div>
                    </th>
                </tr>
            </thead>
            {section.questions.map((q, index) => {
            return (
            <tbody>
                <tr>
                    <td style={{paddingBottom:'20px'}} className="row" key={section.sectionid + "_" + index}>
                        {this.createQuestion(q, section.sectionid)}
                    </td>
                </tr>
            </tbody>
            );
         })}
        </table>
        );
    })}
   </form>
  );
 }
}